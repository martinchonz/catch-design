import React from 'react';
import './sass/App.scss';
import Carousel from './components/Carousel.js'
import Form from './components/Form.js'
import Footer from './components/Footer.js'
import ImageContent from './components/ImageContent.js'

export function App() {
  return (
    <div className="container">
      <header>
        <h1 className="header-title">Front-end test</h1>
      </header>
      <main>
        <Form />
        <ImageContent />
        <Carousel />
      </main>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}


