import React from 'react';

import { Image3, Image4, Image5, Image1 } from '../images/imageStore';
import Slider from "react-slick";
import '../sass/Carousel.scss';

function Carousel() {
  var settings = {
    className: 'Carousel',
    // dots: true,
    arrows: false,
    centerPadding: "60px",
    infinite: false,
    speed: 500,
    slidesToShow: 3.2,
    slidesToScroll: 1,
    centerPadding: "60px",
    responsive: [
      {
        breakpoint: 770,
        settings: {
          slidesToShow: 1.2,
          slidesToScroll: 1,
        }
      },
    ]
  };
  return (
    <Slider {...settings}>
      <div>
        <Image3 />
      </div>
      <div>
        <Image4 />
      </div>
      <div>
        <Image5 />
      </div>
      <div>
        <Image3 />
      </div>
    </Slider>
  );
}

export default Carousel;
