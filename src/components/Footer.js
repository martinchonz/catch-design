import React from 'react';

import '../sass/Footer.scss'; 
import { FacebookIcon, InstagramIcon,  YoutubeIcon } from '../images/imageStore';

function Footer() {
  return (
    <div className="Footer container">
      <div className="wrapper-left">
        <ul>
          <li>
            <a href="https://facebook.com">
              <FacebookIcon />
            </a>
          </li>
          <li>
            <a href="https://instagram.com">
              <InstagramIcon />
            </a>
          </li>
          <li>
            <a href="https://youtube.com">
              <YoutubeIcon />
            </a>
          </li>
        </ul>
        <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
      </div>
      <div className="wrapper-right">
        <div className="location">
          <p className="wrapper-title">Main Office</p>
          <p>6235 Thalia Terrace Apt. 695</p>
          <p>Frederikfurt</p>
          <p>Jamaica</p>
            
        </div>
        <div className="contact">
          <p className="wrapper-title">Contact</p>
          <p>735-421-6378</p>
          <a href="mailto:myname@gmail.com">myname@gmail.com</a>
        </div>
      </div>
    </div>
  )
}
export default Footer;