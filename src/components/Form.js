import React from 'react';

import '../sass/Form.scss';

function Form() {
  return (
    <div className="Form">
      <p className="Form-title">Form title</p>
      <div>
        <div className="wrapper">
          <input type="text" placeholder="First name" />
          <input type="text" placeholder="Last name" />
        </div>
        <div className="wrapper">
          <input type="email" placeholder="Email Address" />
          <select name="" id="">
            <option disabled selected>City</option>
            <option value="auckland">Auckland</option>
            <option value="wellington">Wellington</option>
          </select>
        </div>
      </div>
      <button className="Form-submit-btn Form-submit-btn__primary">Submit</button>
    </div>
  );
}

export default Form;
