import React from 'react';
import {Image1, Image2} from '../images/imageStore'
import '../sass/ImageContent.scss';

function ImageContent() {
  return (
    <div className="ImageContent wrapper">
      <div className="ImageContent-box">
        <Image1 />
        <p className="ImageContent-title">Pink & Purple Sunset</p>
        <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Vestibulum id ligula porta felis euismod semper.</p>
        <a href="https://google.com">Go to sunset</a>
      </div>
      <div className="ImageContent-box">
        <Image2 />
        <p className="ImageContent-title">Sublime Trees</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sagittis lacus vel augue laoreet…</p>
        <a href="https://google.com">View some more</a>
      </div>
    </div>
  );
}

export default ImageContent;
